import React, { useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row, UncontrolledTooltip, Input, Button, Label, Spinner } from "reactstrap";
import { Link, useLocation } from "react-router-dom";
import { getArticles } from "../redux/Articles/ArticlesActions";
import { truncate } from "../utility/commonFunctions";
import ListDataTable from "../components/listDataTable";
import { history } from "../history";
import { AsyncPaginate } from "react-select-async-paginate";
import { FormattedMessage } from "react-intl";
import { getCategoriesDDLAsync } from "../redux/Categories/CategoriesActions";
import { getSourcesDDLAsync } from "../redux/Sources/SourcesActions";
import { getAuthorsDDLAsync } from "../redux/Authors/AuthorsActions";
import AppDatePicker from "../components/AppDatePicker";
import moment from "moment";
import { getLocalItem } from '../utility/commonFunctions'
import { prepareRequestParam } from '../utility/articleParams'
import '../assets/css/datatable.css'

const Articles = () => {
  const { loaders, articles } = useSelector((state) => state);
  const dispatch = useDispatch();
  const query = new URLSearchParams(useLocation().search);

  // Table Data to be got from server
  const [data, setData] = useState(() => []);
  const [meta, setMeta] = useState(() => null);
  const [keyword, setkeyword] = useState(() => "");
  const [category, setCategory] = useState(() => getLocalItem('categories') ?? []);
  const [requestCategories, setRequestCategories] = useState(() => []);
  const [source, setSource] = useState(() => getLocalItem('sources') ?? []);
  const [author, setAuthor] = useState(() => getLocalItem('authors') ?? []);
  const [requestSources, setRequestSources] = useState(() => []);
  const [requestAuthors, setRequestAuthors] = useState(() => []);
  const [dateParams, setDateParams] = useState(() => null);
  const [requestFromDate, setRequestFromDate] = useState(() => null);
  const [requestToDate, setRequestToDate] = useState(() => null);

  // cell width
  const minWidth = "150px";

  const requestParams = useCallback((page, updatedData) => {
    let categoriesParameter = prepareRequestParam('categories', updatedData?.category ?? category);
    let authorsParameter = prepareRequestParam('authors', updatedData?.author ?? author);
    let sourcesParameter = prepareRequestParam('sources', updatedData?.source ?? source);

    setRequestCategories(categoriesParameter);
    setRequestAuthors(authorsParameter);
    setRequestSources(sourcesParameter);

    return {
      page,
      ...sourcesParameter,
      ...authorsParameter,
      ...categoriesParameter,
      date_from: updatedData?.date_from ?? requestFromDate,
      date_to: updatedData?.date_to ?? requestToDate,
      keyword: updatedData?.keyword ?? keyword,
    }
  }, [requestFromDate, requestToDate, author, category, source, keyword]);

  useEffect(() => {
    console.log(requestCategories);
    dispatch(getArticles(requestParams(query.get("page") ? +query.get("page") : 1)));
  }, [query.get("page")]);

  // Table Columns
  const columns = [
    {
      name: "Title",
      selector: "title",
      minWidth,
      cell: (row) => (
        <span id={`title-${row.id}`}>
          {truncate(row.title, 100)}
          <UncontrolledTooltip placement="top" target={`title-${row.id}`}>
            {row.title}
          </UncontrolledTooltip>
        </span>
      ),
    },
    {
      name: "Source",
      selector: "source",
      minWidth,
      center: true,
      cell: (row) => <span id={`source-${row.id}`}>{row.source?.name}</span>,
    },
    {
      name: "Category",
      selector: "category",
      minWidth,
      center: true,
      cell: (row) => <span id={`category-${row.id}`}>{row.category?.name}</span>,
    },
    {
      name: "Author",
      selector: "author",
      minWidth,
      center: true,
      cell: (row) => <span id={`author-${row.id}`}>{row.author?.name}</span>,
    },
  ];

  // bind data to the `data` constant to be displayed
  useEffect(() => {
    if (articles.articlesList) {
      const listData = [];
      articles.articlesList.data.forEach((ele) => {
        const data = {
          id: ele.id,
          title: ele.title,
          source: ele.source,
          category: ele.category,
          author: ele.author,
        };
        if (ele.title) {
          ele.title.trim();
        }
        listData.push(data);
      });
      setData(listData);
      setMeta(articles.articlesList.meta);
    }
  }, [articles.articlesList]);

  const handleFilterCategory = (e) => {
    setCategory(e);

    if (!e) {
      history.push(`/?page=1`);
    }

    dispatch(getArticles(requestParams(1, {category: e})));
  };

  const handleFilterSource = (e) => {
    setSource(e);

    if (!e) {
      history.push(`/?page=1`);
    }
    
    dispatch(getArticles(requestParams(1, {source: e})));
  };

  const handleFilterAuthor = (e) => {
    setAuthor(e);

    if (!e) {
      history.push(`/?page=1`);
    }

    dispatch(getArticles(requestParams(1, {author: e})));
  };

  const handleFilterDate = (date) => {
      if (date.length > 1) {
        const fromOffset = moment(date[0]).utcOffset();
        const toOffset = moment(date[1]).utcOffset();
        const DATE = {
          from_date: moment(date[0]).utc(fromOffset).unix(),
          to_date: moment(date[1]).utc(toOffset).unix(),
        };

        setDateParams([
          new Date(+DATE.from_date * 1000),
          new Date(+DATE.to_date * 1000),
        ]);

        setRequestFromDate(DATE.from_date)
        setRequestToDate(DATE.to_date)

        dispatch(getArticles(1, {date_from: DATE.from_date, date_to: DATE.to_date}));
      }
  };

  const handlePagination = (page) => {
    // getting data from server using params
    dispatch(getArticles(requestParams(page.selected + 1)));

    history.push(`/?page=${page.selected + 1}`);
  };

  const handleFilter = (keyword) => {
    dispatch(getArticles({ page: 1, keyword, ...requestSources, ...requestCategories, ...requestAuthors, date_from: requestFromDate, date_to: requestToDate, }));
    dispatch(getArticles(requestParams(1, {keyword})));
  };

  const renderList = () => {
    if (articles?.articlesList) {
      return (
        <>
          <Row>
            <Col sm="3">
              <Link className="btn btn-primary btn-sm m-2" to="/preferences">Update Preferences</Link>
            </Col>
          </Row>
          <Row className="m-2">
            <Col sm="12">
              <div className="data-list-header d-flex justify-content-between flex-wrap">
                <div className="actions-left d-flex flex-wrap">
                  <div>
                    <Row>
                      <Col sm="4">
                        <Label for="categories" className="mb-1">
                          <FormattedMessage id="Keyword" />*
                        </Label>
                        <Input
                          type="text"
                          value={keyword}
                          placeholder=" Search by keyword..."
                          onChange={(e) => {
                            setkeyword(e.target.value);
                          }}
                          onKeyPress={(event) => {
                            if (event.key === "Enter") {
                              handleFilter(event.target.value);
                            }
                          }}
                        />
                      </Col>
                      <Col lg="4" md="4" sm="4">
                        <Label for="categories" className="mb-1">
                          <FormattedMessage id="Categories" />*
                        </Label>
                        <AsyncPaginate
                          id="categories"
                          loadOptions={getCategoriesDDLAsync}
                          onChange={handleFilterCategory}
                          debounceTimeout={1000}
                          isLoading={loaders.getCategoriesDDLAsync}
                          additional={{
                            page: 1,
                          }}
                          isClearable
                          isMulti
                          value={category}
                          placeholder={
                            loaders.getCategoriesDDLAsync ? (
                              <span>
                                Loading... <Spinner style={{ height: 20, width: 20 }} />
                              </span>
                            ) : (
                              "Search Categories"
                            )
                          }
                        />
                      </Col>
                      <Col lg="4" md="4" sm="4">
                        <Label for="sources" className="mb-1">
                          <FormattedMessage id="Sources" />*
                        </Label>
                        <AsyncPaginate
                          id="sources"
                          loadOptions={getSourcesDDLAsync}
                          onChange={handleFilterSource}
                          debounceTimeout={1000}
                          isLoading={loaders.getSourcesDDLAsync}
                          additional={{
                            page: 1,
                          }}
                          isClearable
                          isMulti
                          value={source}
                          placeholder={
                            loaders.getSourcesDDLAsync ? (
                              <span>
                                Loading... <Spinner style={{ height: 20, width: 20 }} />
                              </span>
                            ) : (
                              "Search Sources"
                            )
                          }
                        />
                      </Col>
                      <Col lg="4" md="4" sm="4">
                        <Label for="authors" className="mb-1">
                          <FormattedMessage id="Authors" />*
                        </Label>
                        <AsyncPaginate
                          id="authors"
                          loadOptions={getAuthorsDDLAsync}
                          onChange={handleFilterAuthor}
                          debounceTimeout={1000}
                          isLoading={loaders.getAuthorsDDLAsync}
                          additional={{
                            page: 1,
                          }}
                          isClearable
                          isMulti
                          value={author}
                          placeholder={
                            loaders.getAuthorsDDLAsync ? (
                              <span>
                                Loading... <Spinner style={{ height: 20, width: 20 }} />
                              </span>
                            ) : (
                              "Search Authors"
                            )
                          }
                        />
                      </Col>
                      <Col lg={4} className="mb-2">
                        <Label for="date" className="mb-1">
                          <FormattedMessage id="Date" />
                        </Label>
                        <AppDatePicker
                          id="date"
                          isClearable
                          disabled={loaders.getOfflineOrders}
                          placeholder="Select Date"
                          value={dateParams}
                          onChange={handleFilterDate}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Button
                          className="add-new-btn"
                          color="primary"
                          onClick={() => handleFilter(keyword)}
                        >
                          <span className="align-middle">Search</span>
                        </Button>
                      </Col>
                    </Row>
                  </div>
                </div>
              </div>
            </Col>
            <Col sm="12">
              <ListDataTable
                columns={columns}
                data={data}
                meta={meta}
                handlePagination={handlePagination}
                onRowClicked={(row) => {
                  history.push(`/article/${row.id}`);
                  history.go(`/article/${row.id}`);
                }}
              />
            </Col>
          </Row>
        </>
      );
    }

  };

  return (
    <>
      {renderList()}
    </>
  );
};

export default Articles;