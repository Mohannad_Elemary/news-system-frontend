import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import LazyButton from "../components/lazyButton";
import { connect } from "react-redux";
import { login } from "../redux/Auth/AuthActions";
import { removeServerError } from "../redux/serverErrors/serverErrorsActions";
import { AsyncPaginate } from "react-select-async-paginate";
import { FormattedMessage } from "react-intl";
import { getCategoriesDDLAsync } from "../redux/Categories/CategoriesActions";
import { getLocalItem } from '../utility/commonFunctions'
import { getSourcesDDLAsync } from "../redux/Sources/SourcesActions";
import { getAuthorsDDLAsync } from "../redux/Authors/AuthorsActions";
import { updatePreferences } from "../redux/Preferences/PreferencesActions";
import { useDispatch } from "react-redux";
import { prepareRequestParam } from '../utility/articleParams'

import {
  Card,
  CardBody,
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Spinner,
} from "reactstrap";

const Preferences = ({
  loaders,
}) => {
  const [category, setCategory] = useState(() => getLocalItem('categories') ?? []);
  const [source, setSource] = useState(() => getLocalItem('sources') ?? []);
  const [author, setAuthor] = useState(() => getLocalItem('authors') ?? []);
  const [requestCategories, setRequestCategories] = useState(() => []);
  const [requestSources, setRequestSources] = useState(() => []);
  const [requestAuthors, setRequestAuthors] = useState(() => []);
  const dispatch = useDispatch();

  const initialValues = {
    categories: [],
    authors: [],
    sources: [],
  };

  useEffect(() => {
    setRequestCategories(prepareRequestParam('categories', category));
    setRequestAuthors(prepareRequestParam('authors', author));
    setRequestSources(prepareRequestParam('sources', source));

  }, [author, category, source]);

  const onSubmit = () => {
    dispatch(updatePreferences({
      ...requestCategories,
      ...requestSources,
      ...requestAuthors,
      category,
      author,
      source,
    }));
  };

  const formik = useFormik({
    initialValues,
    onSubmit,
  });

  const handleFilterCategory = (e) => {
    setCategory(e);
    setRequestCategories(prepareRequestParam('categories', e ?? []));
  };

  const handleFilterSource = (e) => {
    setSource(e);
    setRequestCategories(prepareRequestParam('sources', e ?? []));
  };

  const handleFilterAuthor = (e) => {
    setAuthor(e);
    setRequestCategories(prepareRequestParam('authors', e ?? []));
  };

  return (
    <Row className="m-0 justify-content-center">
      <Col className="d-flex justify-content-center">
          <Row className="m-0">
            {/* Form */}
            <Col className="p-0">
              <Card className="rounded-0 mb-0 p-2">
                <CardBody>
                  <h1>Update Preferences</h1>
                  <Form onSubmit={formik.handleSubmit}>
                    <FormGroup className="form-label-group position-relative has-icon-left">
                    <Label for="categories" className="mb-1">
                        <FormattedMessage id="Categories" />
                      </Label>
                      <AsyncPaginate
                        id="categories"
                        loadOptions={getCategoriesDDLAsync}
                        onChange={handleFilterCategory}
                        debounceTimeout={1000}
                        isLoading={loaders.getCategoriesDDLAsync}
                        additional={{
                          page: 1,
                        }}
                        isClearable
                        isMulti
                        value={category}
                        placeholder={
                          loaders.getCategoriesDDLAsync ? (
                            <span>
                              Loading... <Spinner style={{ height: 20, width: 20 }} />
                            </span>
                          ) : (
                            "Search Categories"
                          )
                        }
                      />
                    </FormGroup>
                    <FormGroup className="form-label-group position-relative has-icon-left">
                      <Label for="sources" className="mb-1">
                        <FormattedMessage id="Sources" />*
                      </Label>
                      <AsyncPaginate
                        id="sources"
                        loadOptions={getSourcesDDLAsync}
                        onChange={handleFilterSource}
                        debounceTimeout={1000}
                        isLoading={loaders.getSourcesDDLAsync}
                        additional={{
                          page: 1,
                        }}
                        isClearable
                        isMulti
                        value={source}
                        placeholder={
                          loaders.getSourcesDDLAsync ? (
                            <span>
                              Loading... <Spinner style={{ height: 20, width: 20 }} />
                            </span>
                          ) : (
                            "Search Sources"
                          )
                        }
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label for="authors" className="mb-1">
                        <FormattedMessage id="Authors" />*
                      </Label>
                      <AsyncPaginate
                        id="authors"
                        loadOptions={getAuthorsDDLAsync}
                        onChange={handleFilterAuthor}
                        debounceTimeout={1000}
                        isLoading={loaders.getAuthorsDDLAsync}
                        additional={{
                          page: 1,
                        }}
                        isClearable
                        isMulti
                        value={author}
                        placeholder={
                          loaders.getAuthorsDDLAsync ? (
                            <span>
                              Loading... <Spinner style={{ height: 20, width: 20 }} />
                            </span>
                          ) : (
                            "Search Authors"
                          )
                        }
                      />
                    </FormGroup>
                    <div className="d-flex justify-content-between">
                      <LazyButton label="Update" loader={loaders.PreferencesBtn} />
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
      </Col>
    </Row>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  serverErrors: state.serverErrors,
  loaders: state.loaders,
});

const mapDispatchToProps = {
  login,
  removeServerError,
};

export default connect(mapStateToProps, mapDispatchToProps)(Preferences);