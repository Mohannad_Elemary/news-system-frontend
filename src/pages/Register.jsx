import * as Yup from "yup";
import { useFormik } from "formik";
import { Link } from "react-router-dom";
import LazyButton from "../components/lazyButton";
import { connect } from "react-redux";
import { register } from "../redux/Register/RegisterActions";
import { removeServerError } from "../redux/serverErrors/serverErrorsActions";

import {
  Card,
  CardBody,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormFeedback,
  UncontrolledAlert,
} from "reactstrap";

const Register = ({
  register,
  serverErrors,
  removeServerError,
  loaders,
}) => {
  const initialValues = {
    name: "",
    email: "",
    password: "",
    password_confirmation: "",
    rememberMe: false,
  };
  const onSubmit = (values) => {
    const user = {
      name: values.name,
      email: values.email,
      password: values.password,
      password_confirmation: values.password_confirmation,
    };
    register(user);
  };
  const validationSchema = Yup.object({
    name: Yup.string().required("This field is required!"),
    email: Yup.string().email("Invalid email format").required("This field is required!"),
    password: Yup.string().required("This field is required!"),
    password_confirmation: Yup.string().required("This field is required!").when(['password'], (password) =>{
      if (password) {
          return Yup.string()
              .equals(password)
              .typeError('Passwords are not matching')
      }
    })
  });

  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
  });

  const handleInputChange = (e, inputName) => {
    formik.handleChange(e);
    if (serverErrors[inputName]) removeServerError(inputName);
  };

  return (
    <Row className="m-0 justify-content-center">
      <Col className="d-flex justify-content-center">
          <Row className="m-0">
            {/* Form */}
            <Col className="p-0">
              <Card className="rounded-0 mb-0 p-2">
                <CardBody>
                  <h1>Register</h1>
                  <Form onSubmit={formik.handleSubmit}>
                  <FormGroup className="form-label-group position-relative has-icon-left">
                      <Input
                        invalid={formik.errors.name && formik.touched.name}
                        type="name"
                        name="name"
                        placeholder="Name"
                        {...formik.getFieldProps("name")}
                        onChange={(e) => {
                          handleInputChange(e, "name");
                        }}
                      />
                      {/* Front end validation */}
                      <FormFeedback>{formik.touched.name && formik.errors.name}</FormFeedback>
                      {/* Backend validation when check inactive account */}
                      {serverErrors.name && (
                        <FormFeedback className="d-block">
                          <UncontrolledAlert color="danger">
                            {serverErrors.name}
                          </UncontrolledAlert>
                        </FormFeedback>
                      )}
                    </FormGroup>
                    <FormGroup className="form-label-group position-relative has-icon-left">
                      <Input
                        invalid={formik.errors.email && formik.touched.email}
                        type="email"
                        name="email"
                        placeholder="Email"
                        {...formik.getFieldProps("email")}
                        onChange={(e) => {
                          handleInputChange(e, "email");
                        }}
                      />
                      {/* Front end validation */}
                      <FormFeedback>{formik.touched.email && formik.errors.email}</FormFeedback>
                      {/* Backend validation when check inactive account */}
                      {serverErrors.email && (
                        <FormFeedback className="d-block">
                          <UncontrolledAlert color="danger">
                            {serverErrors.email}
                          </UncontrolledAlert>
                        </FormFeedback>
                      )}
                    </FormGroup>
                    <FormGroup className="form-label-group position-relative has-icon-left">
                      <Input
                        invalid={formik.errors.password && formik.touched.password}
                        type="password"
                        name="password"
                        placeholder="Password"
                        {...formik.getFieldProps("password")}
                        onChange={(e) => {
                          handleInputChange(e, "password");
                        }}
                      />
                      {/* Front end validation */}
                      <FormFeedback>
                        {formik.touched.password && formik.errors.password}
                      </FormFeedback>
                      {/* Backend validation when check inactive account */}
                      {serverErrors.password && (
                        <FormFeedback className="d-block">
                          <UncontrolledAlert color="danger">
                            {serverErrors.password}
                          </UncontrolledAlert>
                        </FormFeedback>
                      )}
                    </FormGroup>
                    <FormGroup className="form-label-group position-relative has-icon-left">
                      <Input
                        invalid={formik.errors.password_confirmation && formik.touched.password_confirmation}
                        type="password"
                        name="password_confirmation"
                        placeholder="Password Confirmation"
                        {...formik.getFieldProps("password_confirmation")}
                        onChange={(e) => {
                          handleInputChange(e, "password_confirmation");
                        }}
                      />
                      {/* Front end validation */}
                      <FormFeedback>
                        {formik.touched.password_confirmation && formik.errors.password_confirmation}
                      </FormFeedback>
                      {/* Backend validation when check inactive account */}
                      {serverErrors.password_confirmation && (
                        <FormFeedback className="d-block">
                          <UncontrolledAlert color="danger">
                            {serverErrors.password_confirmation}
                          </UncontrolledAlert>
                        </FormFeedback>
                      )}
                    </FormGroup>
                    <FormGroup className="d-flex justify-content-between align-items-center">
                      <div className="float-right">
                        <Link to={"/login"}>
                          Already have account? Login
                        </Link>
                      </div>
                    </FormGroup>
                    <div className="d-flex justify-content-between">
                      <LazyButton label="Signup" loader={loaders.RegisterBtn} />
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
      </Col>
    </Row>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  serverErrors: state.serverErrors,
  loaders: state.loaders,
});

const mapDispatchToProps = {
  register,
  removeServerError,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);