import * as Yup from "yup";
import { useFormik } from "formik";
import { Link } from "react-router-dom";
import LazyButton from "../components/lazyButton";
import { connect } from "react-redux";
import { login } from "../redux/Auth/AuthActions";
import { removeServerError } from "../redux/serverErrors/serverErrorsActions";

import {
  Card,
  CardBody,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormFeedback,
  UncontrolledAlert,
} from "reactstrap";

const Login = ({
  login,
  serverErrors,
  removeServerError,
  loaders,
}) => {
  const initialValues = {
    email: "",
    password: "",
    rememberMe: false,
  };
  const onSubmit = (values) => {
    const user = {
      email: values.email,
      password: values.password,
    };
    login(user);
  };
  const validationSchema = Yup.object({
    email: Yup.string().email("Invalid email format").required("This field is required!"),
    password: Yup.string().required("This field is required!"),
  });

  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
  });

  const handleInputChange = (e, inputName) => {
    formik.handleChange(e);
    if (serverErrors[inputName]) removeServerError(inputName);
  };

  return (
    <Row className="m-0 justify-content-center">
      <Col className="d-flex justify-content-center">
          <Row className="m-0">
            {/* Form */}
            <Col className="p-0">
              <Card className="rounded-0 mb-0 p-2">
                <CardBody>
                  <h1>Login</h1>
                  <p className="mb-2 mt-1">
                    Welcome to Innoscripta! <br /> Please log in using your Email and Password
                  </p>
                  <Form onSubmit={formik.handleSubmit}>
                    <FormGroup className="form-label-group position-relative has-icon-left">
                      <Input
                        invalid={formik.errors.email && formik.touched.email}
                        type="email"
                        name="email"
                        placeholder="Email"
                        {...formik.getFieldProps("email")}
                        onChange={(e) => {
                          handleInputChange(e, "email");
                        }}
                      />
                      {/* Front end validation */}
                      <FormFeedback>{formik.touched.email && formik.errors.email}</FormFeedback>
                      {/* Backend validation when check inactive account */}
                      {serverErrors.email && (
                        <FormFeedback className="d-block">
                          <UncontrolledAlert color="danger">
                            {serverErrors.email}
                          </UncontrolledAlert>
                        </FormFeedback>
                      )}
                    </FormGroup>
                    <FormGroup className="form-label-group position-relative has-icon-left">
                      <Input
                        invalid={formik.errors.password && formik.touched.password}
                        type="password"
                        name="password"
                        placeholder="Password"
                        {...formik.getFieldProps("password")}
                        onChange={(e) => {
                          handleInputChange(e, "password");
                        }}
                      />
                      {/* Front end validation */}
                      <FormFeedback>
                        {formik.touched.password && formik.errors.password}
                      </FormFeedback>
                      {/* Backend validation when check inactive account */}
                      {serverErrors.password && (
                        <FormFeedback className="d-block">
                          <UncontrolledAlert color="danger">
                            {serverErrors.password}
                          </UncontrolledAlert>
                        </FormFeedback>
                      )}
                    </FormGroup>
                    <FormGroup className="d-flex justify-content-between align-items-center">
                      <div className="float-right">
                        <Link to={"/register"}>
                          New account? sign up
                        </Link>
                      </div>
                    </FormGroup>
                    <div className="d-flex justify-content-between">
                      <LazyButton label="Login" loader={loaders.loginBtn} />
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
      </Col>
    </Row>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  serverErrors: state.serverErrors,
  loaders: state.loaders,
});

const mapDispatchToProps = {
  login,
  removeServerError,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);