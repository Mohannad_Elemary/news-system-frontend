import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row, Card, CardBody} from "reactstrap";
import { viewArticle} from "../redux/Articles/ArticlesActions";
import moment from "moment";
import { useParams } from "react-router-dom";

const ArticleDetails = () => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const { articles } = useSelector((state) => state);


  useEffect(() => {
    dispatch(viewArticle(id));
  }, []);

  const renderArticleDetails = () => {
    if (articles.viewedArticle) {
      return (
        <Row>
          <Col sm="12">
            <Card>
              <CardBody>
                <Row>
                  <Col sm="12">
                    <div className="mt-1">
                      <h4>Title:</h4>
                      <p>{articles.viewedArticle.viewedArticle.title}</p>
                    </div>
                    <hr />
                    <div className="mt-1">
                      <h4>Public Date:</h4>
                      <p>{moment.unix(articles.viewedArticle.viewedArticle.date).format("LLL")}</p>
                    </div>
                    <hr />
                    <div className="mt-1">
                      <h4>Description:</h4>
                      <p>{articles.viewedArticle.viewedArticle.description}</p>
                    </div>
                    <hr />
                    <div className="mt-1">
                      <h4>Source:</h4>
                      <p>{articles.viewedArticle.viewedArticle.source?.name ?? '-'}</p>
                    </div>
                    <hr />
                    <div className="mt-1">
                      <h4>Category:</h4>
                      <p>{articles.viewedArticle.viewedArticle.category?.name ?? '-'}</p>
                    </div>
                    <hr />
                    <div className="mt-1">
                      <h4>Author:</h4>
                      <p>{articles.viewedArticle.viewedArticle.author?.name ?? '-'}</p>
                    </div>
                    <hr />
                    </Col>
                    </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      );
    }
  };

  return  <>{renderArticleDetails()}</>;
};

export default ArticleDetails;