import { axiosInstance } from "../../network/apis";
import { store } from "../storeConfig/store";
import { addServerError } from "../serverErrors/serverErrorsActions";
import { redirect } from '../../utility/globalPages'

export const register = (user) => async (dispatch) => {
  try {
    user.client_id = process.env.REACT_APP_CLIENT_ID;
    user.client_secret = process.env.REACT_APP_CLIENT_SECRET;
  
    await axiosInstance.post("/api/v1/auth/register", user, {
      handlerEnabled: true,
      loader: "RegisternBtn",
    });
    
    redirect('/');
  } catch (err) {
    if (err.response && err.response.data.message)
      store.dispatch(addServerError({ email: err.response.data.message }));
  }
};
