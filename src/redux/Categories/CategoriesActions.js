import { axiosInstance } from "../../network/apis";

export const getCategoriesDDLAsync = async (search, loadedOptions, { page }) => {
  const res = await axiosInstance.get(
    `api/v1/articles/categories?name=${search}&page=${page}`,
    {
      handlerEnabled: true,
      loader: "getCategoriesDDLAsync",
    },
  );

  return {
    options: res.data.data.map((ele) => ({
      value: ele.id,
      label: ele.name,
    })),
    hasMore: page < res.data.meta.last_page,
    additional: {
      page: page + 1,
    },
  };
};
