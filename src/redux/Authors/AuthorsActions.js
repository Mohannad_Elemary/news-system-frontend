import { axiosInstance } from "../../network/apis";

export const getAuthorsDDLAsync = async (search, loadedOptions, { page }) => {
  const res = await axiosInstance.get(
    `api/v1/articles/authors?name=${search}&page=${page}`,
    {
      handlerEnabled: true,
      loader: "getAuthorsDDLAsync",
    },
  );

  return {
    options: res.data.data.map((ele) => ({
      value: ele.id,
      label: ele.name,
    })),
    hasMore: page < res.data.meta.last_page,
    additional: {
      page: page + 1,
    },
  };
};
