import {
  ADD_SERVER_ERROR,
  REMOVE_SERVER_ERROR,
  CLEAR_SERVER_ERRORS,
} from "./serverErrorsTypes";

const INITIAL_STATE = { domainChecked: false };

const errorsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_SERVER_ERROR:
      return { ...state, ...action.payload };
    case CLEAR_SERVER_ERRORS:
      return {};
    case REMOVE_SERVER_ERROR:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default errorsReducer;
