import {
  ADD_SERVER_ERROR,
  REMOVE_SERVER_ERROR,
  CLEAR_SERVER_ERRORS,
} from "./serverErrorsTypes";

export const addServerError = (error) => (dispatch) => {
  dispatch({
    type: ADD_SERVER_ERROR,
    payload: error,
  });
};

export const removeServerError = (errorKey) => (dispatch) => {
  const error = {};
  error[errorKey] = null;
  dispatch({
    type: REMOVE_SERVER_ERROR,
    payload: error,
  });
};
export const clearServerErrors = () => (dispatch) => {
  dispatch({
    type: CLEAR_SERVER_ERRORS,
  });
};