import { axiosInstance } from "../../network/apis";
import { store } from "../storeConfig/store";
import { addServerError } from "../serverErrors/serverErrorsActions";
import { setLocalItem } from '../../utility/commonFunctions'
import { redirect } from '../../utility/globalPages'

export const login = (user) => async (dispatch) => {
  try {
    user.client_id = process.env.REACT_APP_CLIENT_ID;
    user.client_secret = process.env.REACT_APP_CLIENT_SECRET;
  
    let response = await axiosInstance.post("/api/v1/auth/login", user, {
      handlerEnabled: true,
      loader: "loginBtn",
    });

    let authors = response?.data?.data?.settings?.authors.map((item) => ({
      label: item.name,
      value: item.id,
    }));

    let categories = response?.data?.data?.settings?.categories.map((item) => ({
      label: item.name,
      value: item.id,
    }));

    let sources = response?.data?.data?.settings?.sources.map((item) => ({
      label: item.name,
      value: item.id,
    }));

    setLocalItem('authors', authors);
    setLocalItem('categories', categories);
    setLocalItem('sources', sources);

    redirect('/');
  } catch (err) {
    if (err.response && err.response.data.message)
      store.dispatch(addServerError({ email: err.response.data.message }));
  }
};
