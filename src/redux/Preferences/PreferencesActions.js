import { axiosInstance } from "../../network/apis";
import { redirect } from '../../utility/globalPages'
import { store } from "../storeConfig/store";
import { addServerError } from "../serverErrors/serverErrorsActions";
import { setLocalItem } from '../../utility/commonFunctions'

export const updatePreferences = (data) => async (dispatch) => {
  try {
    await axiosInstance.put("/api/v1/users/settings", data, {
      handlerEnabled: true,
      loader: "PreferencesBtn",
    });

    setLocalItem('authors', data.author);
    setLocalItem('categories', data.category);
    setLocalItem('sources', data.source);

    redirect('/');
  } catch (err) {
    if (err.response && err.response.data.message)
      store.dispatch(addServerError({ email: err.response.data.message }));
  }
};
