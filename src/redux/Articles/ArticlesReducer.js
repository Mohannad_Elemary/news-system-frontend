import {
  GET_ARTICLES,
  VIEW_ARTICLE,
} from "./ArticlesTypes";

const INITIAL_STATE = {
  articlesList: null,
  viewedArticle: null,
};

const ArticlesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_ARTICLES: {
      return { ...state, articlesList: action.payload };
    }
    case VIEW_ARTICLE: {
      return { ...state, viewedArticle: action.payload };
    }
    default: {
      return state;
    }
  }
};

export default ArticlesReducer;
