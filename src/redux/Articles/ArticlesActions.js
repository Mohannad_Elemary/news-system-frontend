import {
  GET_ARTICLES,
  VIEW_ARTICLE,
} from "./ArticlesTypes";
import { axiosInstance } from "../../network/apis";
import { addLoader, removeLoader } from "../loaders/loadersActions";
import { store } from "../storeConfig/store";

export const getArticles = (params) => async (dispatch) => {
  store.dispatch(addLoader("listArticles"));
  try {
    const res = await axiosInstance.get(
      `/api/v1/articles`,
      {
        handlerEnabled: true,
        params,
      },
    );

    dispatch({
      type: GET_ARTICLES,
      payload: res.data,
    });
    store.dispatch(removeLoader("listArticles"));
  } catch (err) {
    store.dispatch(removeLoader("listArticles"));
  }
};

export const viewArticle = (id) => async (dispatch) => {
  try {
    const res = await axiosInstance.get(`/api/v1/articles/${id}`, {
      handlerEnabled: true,
    });
    dispatch({
      type: VIEW_ARTICLE,
      payload: { viewedArticle: res.data.data },
    });
  } catch (err) {
    dispatch({
      type: VIEW_ARTICLE,
      payload: { viewedArticle: null },
    });
  }
};