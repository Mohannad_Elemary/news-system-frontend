import { combineReducers } from "redux";
import articlesReducer from "./Articles/ArticlesReducer";
import errorsReducer from "./serverErrors/serverErrorsReducer";
import loadersReducer from "./loaders/loadersReducer";

const rootReducer = combineReducers({
  serverErrors: errorsReducer,
  loaders: loadersReducer,
  articles: articlesReducer,
});

export default rootReducer;
