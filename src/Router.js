import Login from "./pages/Login";
import Articles from "./pages/Articles";
import ArticleDetails from "./pages/ArticleDetails";
import Register from "./pages/Register";
import Preferences from "./pages/Preferences";
import { createBrowserRouter } from "react-router-dom";

export const Router = () => {
  return createBrowserRouter([
    {
      path: "/",
      element: <Articles />,
    },
    {
      path: "/login",
      element: <Login />,
    },
    {
      path: "/register",
      element: <Register />,
    },
    {
      path: "article/:id",
      element: <ArticleDetails />
    },
    {
      path: "preferences",
      element: <Preferences />
    }
  ]);
}

export default Router;
