/* eslint-disable no-param-reassign */
import { history } from "../history";
import { duration } from "./constants";

export const getDomain = () => {
  const { host } = window.location;
  const domain = host.split(".")[0];
  const domain_name = process.env.REACT_APP_DOMAIN ? process.env.REACT_APP_DOMAIN : domain;
  return domain_name;
};

/**
 *
 * @param key Local Storage Key
 * @param value Local Storage Value (String)
 * @param ttl Time to live (Expiry Date in MS)
 */
export const setLocalItem = (key, value, ttl = duration.week) => {
  const now = new Date();

  // `item` is an object which contains the original value
  // as well as the time when it's supposed to expire
  const item = {
    value,
    expiry: now.getTime() + ttl,
  };
  localStorage.setItem(key, JSON.stringify(item));
};
/**
 *
 * @param key Local Storage Key
 */
export const getLocalItem = (key) => {
  const itemStr = localStorage.getItem(key);
  // if the item doesn't exist, return null
  if (!itemStr) {
    return null;
  }
  const item = JSON.parse(itemStr);
  const now = new Date();
  // compare the expiry time of the item with the current time
  if (now.getTime() > item.expiry) {
    // If the item is expired, delete the item from storage
    // and return null
    localStorage.removeItem(key);
    return null;
  }
  return item.value;
};

export const truncate = (text, num = 10) => {
  if (text.length > num) {
    return `${text.substring(0, num - 3)}...`;
  }
  return text;
};

export const debounce = (func, wait, immediate) => {
  let timeout;
  return (...args) => {
    const context = this;
    const later = () => {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};
