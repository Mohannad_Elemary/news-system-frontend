/* eslint-disable no-param-reassign */
import { history } from "../history";

export const redirect = (page) => {
  history.push(page);
  history.go(page);
};
