/* eslint-disable no-param-reassign */

export const prepareRequestParam = (paramName, data) => {
  return data.reduce(function(accum, currentVal, index) {
    accum[`${paramName}[${index}]`] = currentVal.value;
    return accum;
  }, {});
};
